#!/bin/env bash

PROC_ENTRY=/proc/version_signature

if [ -e ${PROC_ENTRY} ];then
  echo "Success: it's $(cat ${PROC_ENTRY})"
  RET=0
else
  echo "Error!!"
  RET=1
fi

exit ${RET}
